Meteor.publish('listPosts', function(){
    return Posts.find({ "publishAt": { $lte: new Date()}},{"full_body": 0, sort: {"publishAt": -1}});
});

Meteor.publish('listMyPosts', function(){
    return Posts.find({ "userId": this.userId},{"full_body": 0, sort: {"publishAt": -1}});
});