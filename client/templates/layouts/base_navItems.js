Handlebars.registerHelper('navLink', function(pageName, linkName) {
    var ret =
        '<li' + ((Router.current().route.getName() == pageName) ? ' class="active"' : '') + '>' +
            '<a href="' + Router.routes[pageName].path() + '">' + linkName + '</a>' +
        '</li>';
    return new Handlebars.SafeString(ret);
});
