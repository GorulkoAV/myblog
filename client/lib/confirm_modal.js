/* Using confirmModal      Default options
 * ----------------------  --------------------
 *	$('#myLinkToConfirm').confirmModal({
 *		confirmTitle         : 'Please confirm',
 *		confirmMessage       : 'Are you sure you want to perform this action ?',
 *		confirmOk            : 'Yes',
 *		confirmCancel        : 'Cancel',
 *		confirmDirection     : 'rtl',
 *		confirmStyle         : 'primary',
 *		confirmCallback      : defaultCallback,
 *		confirmDismiss       : true,
 *		confirmAutoOpen      : false
 *		confirmConditionOpen : defaultConditionOpen
 *	});
 * 
 * Name 			    Type 		        Description
 * -----------------	----------------	-----------------------------
 * confirmTitle		    string|function 	Title of the confirm modal.
 * confirmMessage	    string|function 	Message of the confirm modal.
 * confirmOk 		    string              Validation button's message.
 * confirmCancel 	    string 		        Cancelling button's message.
 * confirmDirection	    string 		        Direction for showing buttons, starting with the validation button.
 * confirmStyle	 	    string 		        Style of the validation button.
 * confirmCallback 	    function	        Function to execute when validate modal.
 * confirmDismiss 	    boolean 	        If modal is dismiss when confirm.
 * confirmAutoOpen	    boolean 	        If modal is automatically open when created.
 * confirmConditionOpen function            Open when value is true
 */

(function($) {
   $.fn.confirmModal = function(opts)
   {
       var body = $('body');
       var defaultOptions    = {
           confirmTitle         : 'Please confirm',
           confirmMessage       : 'Are you sure you want to perform this action ?',
           confirmOk            : 'Yes',
           confirmCancel        : 'Cancel',
           confirmDirection     : 'rtl',
           confirmStyle         : 'primary',
           confirmCallback      : defaultCallback,
           confirmDismiss       : true,
           confirmAutoOpen      : false,
           confirmConditionOpen : defaultConditionOpen
       };
       var options = $.extend(defaultOptions, opts);

       var headModalTemplate =
           '<div class="modal fade" id="#modalId#" tabindex="-1" role="dialog" aria-labelledby="#AriaLabel#" aria-hidden="true">' +
               '<div class="modal-dialog">' +
                   '<div class="modal-content">' +
                       '<div class="modal-header">' +
                           '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                           '<h3 id="modalHeading">#Heading#</h3>' +
                       '</div>' +
                       '<div class="modal-body">' +
                           '<p id="modalBody">#Body#</p>' +
                       '</div>' +
                       '<div class="modal-footer">' +
                       '#buttonTemplate#' +
                       '</div>' +
                   '</div>' +
               '</div>' +
           '</div>'
           ;

       return this.each(function(index)
       {
           var confirmLink = $(this);
           var targetData  = confirmLink.data();

           var currentOptions = $.extend(options, targetData);

           var modalId = "confirmModal" + Math.floor(Math.random()*(1e+9));
           var modalTemplate = headModalTemplate;
           var buttonTemplate =
               '<button class="btn btn-default" data-dismiss="modal">#Cancel#</button>' +
               '<button class="btn btn-#Style#" data-dismiss="ok">#Ok#</button>'
               ;

           if(options.confirmDirection == 'ltr')
           {
               buttonTemplate =
                   '<button class="btn btn-#Style#" data-dismiss="ok">#Ok#</button>' +
                   '<button class="btn btn-default" data-dismiss="modal">#Cancel#</button>'
               ;
           }

           var confirmTitle = '';
           if(typeof options.confirmTitle == 'string')
           {
               confirmTitle = options.confirmTitle;
           }

           var confirmMessage = '';
           if(typeof options.confirmMessage == 'string')
           {
               confirmMessage = options.confirmMessage;
           }

           modalTemplate = modalTemplate.
               replace('#buttonTemplate#', buttonTemplate).
               replace('#modalId#', modalId).
               replace('#AriaLabel#', confirmTitle).
               replace('#Heading#', confirmTitle).
               replace('#Body#', confirmMessage).
               replace('#Ok#', options.confirmOk).
               replace('#Cancel#', options.confirmCancel).
               replace('#Style#', options.confirmStyle)
           ;

           body.append(modalTemplate);

           var confirmModal = $('#' + modalId);

           confirmLink.on('click', function(modalEvent)
           {
               modalEvent.preventDefault();

               if(typeof options.confirmTitle == 'function')
               {
                   $('#' + modalId + ' #modalHeading').eq(0).html(options.confirmTitle.call(this));
               }
               if(typeof options.confirmMessage == 'function')
               {
                   $('#' + modalId + ' #modalBody').eq(0).html(options.confirmMessage.call(this));
               }
               if(typeof options.confirmConditionOpen == 'function') {
                   if (options.confirmConditionOpen(confirmLink, confirmModal)) {
                       confirmModal.modal('show');
                   }
               }
           });

           $('button[data-dismiss="ok"]', confirmModal).on('click', function(event) {
               if (options.confirmDismiss) {
                   confirmModal.modal('hide');
               }
               options.confirmCallback(confirmLink, confirmModal);
           });

           if (options.confirmAutoOpen) {
               confirmModal.modal('show');
           }
       });

       function defaultCallback(target, modal)
       {
           window.location = $(target).attr('href');
       }

       function defaultConditionOpen(target, modal)
       {
           return true;
       }
   };
})(jQuery);
