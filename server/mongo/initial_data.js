// On server startup, if the database is empty, create some initial test data.
if (Meteor.isServer) {
    Meteor.startup(function () {
        if (Posts.find().count() === 0) {
            var _user_id = Accounts.createUser({
                username: 'user',
                email: 'user@gmail.com',
                password: '123456',
                profile: {
                    name: 'user name'
                }
            });
            var _user = Meteor.users.findOne(_user_id);
            for (var i = 0; i < 10; i++) {
                var _new_post = {
                    title      :  Fake.sentence(10),
                    short_body :  Fake.paragraph(5),
                    full_body  :  Fake.paragraph(100),
                    author     :  _user.username,
                    publishAt  :  moment().subtract(1, 'days').toDate(),
                    createdAt  :  new Date(),
                    userId     :  _user.id
                };
                Posts.insert(_new_post)
            }
        }
    });
}
