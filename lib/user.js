(function(global) {
    
    var _user = {};
    
    _user.id = function()
    {
    	var mu = Meteor.user();
		if (!mu) return null;
		return mu._id;
	} 
	
	_user.name = function()
	{
		var mu = Meteor.user();
		if (!mu || !mu.emails || !mu.emails[0] || !mu.emails[0].address) return null;
		return mu.emails[0].address;
	}

    global.user = _user;
    
})(this);