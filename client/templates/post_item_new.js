Template.post_item_new.onRendered(function ()
{
    // The formats values of the fields in the form

    var dtpFormat = 'DD.MM.YYYY HH:mm';

    // Add datetimepicker for field #dtpPublishAt

    this.$('#dtpPublishAt').datetimepicker(
        {
            defaultDate: new Date(),
            format: dtpFormat,
            showTodayButton: true,
            allowInputToggle: true
        });

    // Add confirmation dialog to action button #btnSave

    var confirmModalSavePost = {
        confirmTitle: '<p class="text-primary">Confirm add new post</p>',
        confirmMessage: getConfirmMessage,
        confirmConditionOpen: isValidForm,
        confirmCallback: savePost
    };
    this.$('#btnSave').confirmModal(confirmModalSavePost);

    // Function for getting/setting the values of the form fields

    var postTitle = this.$('#inpTitle');
    postTitle.getValue = function() { return postTitle.val(); };
    postTitle.setValue = function(str) { postTitle.val(str); };

    var postShortBody = this.$('#inpShortBody');
    postShortBody.getValue = function() { return postShortBody.val(); };
    postShortBody.setValue = function(str) { postShortBody.val(str); };

    var postFullBody = this.$('#inpFullBody');
    postFullBody.getValue = function() { return postFullBody.val(); };
    postFullBody.setValue = function(str) { postFullBody.val(str); };

    var postAuthor = this.$('#lblAuthor');
    postAuthor.setValue = function(str) {postAuthor.eq(0).html(str); };
    postAuthor.getValue = function() { return postAuthor.eq(0).html(); };

    var postPublishAt = this.$('#dtpPublishAt').data("DateTimePicker");
    postPublishAt.getMoment = function() { return postPublishAt.date(); };
    postPublishAt.setNow = function() { postPublishAt.date(new Date()); };

    // Set default values for form fields

    postAuthor.setValue((user.id()) ? user.name() : 'undefined');

    // Validation rules for form new post

    var formNewPost = this.$('#formNewPost');
    formNewPost.validate({
        rules: {
            inpTitle: {
                required: true
            },
            inpShortBody: {
                required: true
            },
            inpFullBody: {
                required: true
            }
        },
        messages: {
            inpTitle: {
                required: 'Please enter title to post.'
            },
            inpShortBody: {
                required: 'Please enter a short text body of the post to display it in the list of posts.'
            },
            inpFullBody: {
                required: 'Please enter a full text body of the post.'
            }
        },
        onsubmit: false
    });

    // Dynamic message for confirmation dialogs

    function getConfirmMessage()
    {
        var message = 'Are you sure you want to add this post?';

        var messageTemplateNow = '</br></br>' +
            '<p class="text-success">' +
                'Your post will be published immediately.' +
            '</p>';

        var messageTemplateSince = '</br></br>' +
            '<p class="text-danger">' +
                'Attention! Your post will be published since #publishAt#' +
            '</p>';

        if (moment().isAfter(postPublishAt.getMoment())) {
            message += messageTemplateNow;
        } else {
            message += messageTemplateSince;
        }
        message = message.
            replace('#publishAt#', postPublishAt.getMoment().format(dtpFormat));

        return message;
    };

    // Checking form on valid values fields (true/false)

    function isValidForm() {
        return formNewPost.valid();
    }

    // Save new post

    function savePost() {

        var newPost = {
            title      :  postTitle.getValue(),
            short_body :  postShortBody.getValue(),
            full_body  :  postShortBody.getValue(),
            author     :  postAuthor.getValue(),
            publishAt  :  postPublishAt.getMoment().toDate()
        };

        var newPost = Meteor.call('addPost', newPost);

        postTitle.setValue('');
        postShortBody.setValue('');
        postFullBody.setValue('');
        postPublishAt.setNow();

        return newPost;
    };
});

Template.post_item_new.events({
    'submit form': function(e) {
        e.preventDefault();
    }
});