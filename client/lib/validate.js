$.validator.setDefaults({
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'div',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else if (element.parent('.radio-inline').length) {
            error.insertAfter(element.parent().parent());
        } else {
            error.insertAfter(element);
        }
    },
    invalidHandler: function(event, validator) {
        $('.submit').attr('class', 'submit btn btn-danger disabled');
    },
    onkeyup: function( element, event ) {
        this.form();
        if (this.valid()) { // checks form for validity
            $('.submit').attr('class', 'submit btn btn-success');
        } else {
            $('.submit').attr('class', 'submit btn btn-danger disabled');
        }
    },
    onclick: function( element ) {
        this.form();
        if (this.valid()) { // checks form for validity
            $('.submit').attr('class', 'submit btn btn-success');
        } else {
            $('.submit').attr('class', 'submit btn btn-danger disabled');
        }
        return false;
    }
});
