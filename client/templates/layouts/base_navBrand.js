Template.layout_base_navBrand.events({
    'click .navbar-brand': function(e){
        // Unselect navbar-brand link after click.
        // Important in a situation when current page is home and it is not updated.
        $(e.target).blur();
    }
});