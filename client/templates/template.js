Template.registerHelper('formatDate', function(date) {
   return moment(date).format('MMMM DD, YYYY hh:mm A');
});