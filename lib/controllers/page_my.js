controller_page_my = controller_page_base.extend({
    template: 'posts_list',

    waitOn: function() {
        return Meteor.subscribe('listMyPosts');
    },

    data: function(){
        return { posts: Posts.find({}) };
    },

    onBeforeAction: function(){
        if (!Meteor.user()) {
            if (Meteor.loggingIn()) {
                this.render(this.loadingTemplate);
            } else {
                this.render('page_access_denied');
            }
        } else {
            this.next();
        }
    }
});
