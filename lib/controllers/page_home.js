controller_page_home = controller_page_base.extend({
	template: 'posts_list',

	waitOn: function() {
		return Meteor.subscribe('listPosts');
	},

	data: function(){
		return { posts: Posts.find({}) };
	}
});