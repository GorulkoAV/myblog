Meteor.methods(
{
   'addPost': function(post)
   {
      if (!user.id) {
         throw new Meteor.Error(401, 'You need to log in first');
      }
      var additionalParams = {
         createdAt: new Date(),
         userId: user.id()
      }
      _.extend(post, additionalParams);
      post.id = Posts.insert(post);

      return post;
   }
});