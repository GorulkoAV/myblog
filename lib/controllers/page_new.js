controller_page_new = controller_page_base.extend({
	template: 'post_item_new',

	onBeforeAction: function(){
		if (!Meteor.user()) {
			if (Meteor.loggingIn()) {
				this.render(this.loadingTemplate);
			} else {
				this.render('page_access_denied');
			}
		} else {
			this.next();
		}
	}
});