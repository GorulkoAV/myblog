Router.configure({
    layoutTemplate: 'layout_base',
    loadingTemplate: 'spinner',
    notFoundTemplate: 'page_not_found'
});

Router.route('/', {
    name: 'home',
    controller: 'controller_page_home'
});

Router.route('/my', {
    name: 'my',
    controller: 'controller_page_my'
});

Router.route('/new', {
    name: 'new',
    controller: 'controller_page_new'
});

Router.route('/view', {
    name: 'view',
    controller: 'controller_page_view'
});